#include <iostream>
#include <vector>
#include "turoperator.h"
#include "LesData3.h"
#include "turoperatorer.h"  // for finnTuroperator(...)
#include "turopplegg.h"
#include "byer.h"   // for finnBy(...)
# include "const.h" //MINTLF, MAXTLF
using namespace std;

/**
 * Default constructor. 
*/
Turoperator::Turoperator() : telefonnummer(0), oppleggID(1), antall_opplegg(0) {}

/**
 * Constructor med parametre. 
*/
Turoperator::Turoperator(const string& gateadresse,
                        const string& postnr_sted,
                        const string& kontaktperson_navn,
                        const string& mailadresse,
                        const string& webside,
                        int telefonnummer,
                        const vector<Turopplegg*> opplegg,
                        int oppleggID, 
                        int antall_opplegg)
    : gateadresse(gateadresse), postnr_sted(postnr_sted), kontaktperson_navn(kontaktperson_navn), mailadresse(mailadresse), webside(webside), telefonnummer(telefonnummer), opplegg(opplegg), oppleggID(oppleggID), antall_opplegg(antall_opplegg) {}

/**
 * Destructor. 
*/
Turoperator::~Turoperator() {
    for (auto &opplegg : opplegg) {
        delete opplegg; 
    }
    opplegg.clear();
}

/**
 * Funksjon som returnerer det private medlemmet antall opplegg laget av 
 * turoperatoren. Dette måler IKKE vektoren opplegg med size(), ettersom 
 * man kan lese inn antall turopplegg fra fil, og det vil ikke automatisk
 * legge turoppleggene i vektoren. 
 * 
 * @return  int med antall opplegg registrert på turoperatøren. 
*/
int Turoperator::getAntallTuropplegg() const {
    return antall_opplegg;
}

/**
 * Funksjon som returnerer turoperatørens gateadresse. 
 * 
 * @return  string med turoperatørens gateadresse.
*/
string Turoperator::getGateadresse() const {
    return gateadresse;
}

/**
 * Funksjon som returnerer turoperatørens kontaktperson. 
 * 
 * @return  string med navnet til turoperatørens kontaktperson.
*/
string Turoperator::getKontaktperson_navn() const {
    return kontaktperson_navn;
}

/**
 * Funksjon som returnerer turoperatørens mailadresse. 
 * 
 * @return  string med mailadressen til turoperatøren.
*/
string Turoperator::getMailadresse() const {
    return mailadresse;
}

/**
 * Funksjon som returnerer turoperatørens postnummer og sted. 
 * 
 * @return  string med postnummeret og stedet til turoperatøren.
*/
string Turoperator::getPostnr_sted() const {
    return postnr_sted;
}

/**
 * Funksjon som returnerer turoperatørens telefonnummer. 
 * 
 * @return  int med telefonnummeret til turoperatøren.
*/
int Turoperator::getTelefonnummer() const {
    return telefonnummer;
}

/**
 * Funksjon som returnerer turoperatørens webside. 
 * 
 * @return  string med webside-URL til turoperatøren.
*/
string Turoperator::getWebside() const {
    return webside;
}

/**
 * Funksjon som returnerer privat medlemmet oppleggID. 
 * OppleggID teller opp opplegg som er skrevet til fil. Dette er IKKE det samme
 * som antall opplegg av turoperatører, da ikke alle lagde opplegg nødvendigvis
 * skrives til fil. 
 * 
 * @return  int med oppleggID.
*/
int Turoperator::hentOppleggID() const { return oppleggID; }

/**
 * Funksjon som kalles fra Turoperatorer-klassen og tillater bruker å velge
 * hvilket medlem av turoperatøren som skal endres og skrive inn en ny verdi.
*/
void Turoperator::endre() {
    char kommando;
    string input;
    int nummer;

    do {
        cout << "Velg felt du vil endre:\n"
                  << "G - Gateadresse\n"
                  << "P - Postnummer og sted\n"
                  << "K - Kontaktperson\n"
                  << "M - Mailadresse\n"
                  << "W - Webside\n"
                  << "T - Telefonnummer\n"
                  << "Q - Avslutt endring\n";
        kommando = lesChar("Ditt valg");
        
        switch (kommando) {
            case 'G':
                cout << "Ny gateadresse: ";
                getline(cin, gateadresse);
                break;
            case 'P':
                cout << "Nytt postnummer og sted: ";
                getline(cin, postnr_sted);
                break;
            case 'K':
                cout << "Ny kontaktperson: ";
                getline(cin, kontaktperson_navn);
                break;
            case 'M':
                cout << "Ny mailadresse: ";
                getline(cin, mailadresse);
                break;
            case 'W':
                cout << "Ny webside: ";
                getline(cin, webside);
                break;
            case 'T':
                lesInt("Nytt telefonnummer: ", MINTLF, MAXTLF);
                break;
            case 'Q':
                // Avslutter loopen
                break;
            default:
                cout << "Ugyldig valg. Vennligst prøv igjen.\n";
                break;
        }
    } while (kommando != 'Q');

    cout << "Endringer fullført.\n";
}

/**
 * Funksjon som brukes i forbindelse med utskrift av fil ved turopplegg. 
 * Funksjonen inkrementerer oppleggID, som teller antall gangen turoperatøren 
 * har skrevet til fil. 
*/
void Turoperator::inkrementerOppleggID() { ++oppleggID; }

/**
 * Funksjon som oppretter et nytt opplegg, kaller turopplegg-klassens
 * planOpplegg-funksjon og så legger det til i oppleggvektoren til
 * turoperatoren.
 * 
 * @param   turoperator_navn    -   Navnet på turoperatøren sendes med for 
 *                                  å hjelpe Turopplegg-klassen med utskrift.
 * @param   oppleggID           -   Sendes med fra Turoperatorer::lagOpplegg
 *                                  og sendes videre til Turopplegg::planOpplegg
 *                                  som så bruker den til filnavnet på utskrift.
 * @see Turopplegg::Turopplegg()
 * @see Turopplegg::planOpplegg(...)
 * @see Turoperator::inkrementerOppleggID()
*/
void Turoperator::leggTilOpplegg(const string& turoperator_navn, int oppleggID) {
    Turopplegg* nyttOpplegg = new Turopplegg(); // Oppretter før funksjonskall.
    bool skrevetUt = nyttOpplegg->planOpplegg(nyttOpplegg, turoperator_navn, oppleggID); 
    // Sjekker at det turopplegget faktisk ble opprettet. 
    // Hvis det ikke er byer å legge til, vil Turopplegg::planOpplegg
    // ikke modifisere nyttOpplegg og dermed forblir den nullptr.
    if (nyttOpplegg == nullptr) {
        delete nyttOpplegg;
        
    } else {
        opplegg.push_back(nyttOpplegg);
        ++antall_opplegg;   // Inkrementerer antall_opplegg. 
        if (skrevetUt)
            inkrementerOppleggID();
    }
}

/**
 * Funksjon som skriver ut alle private medlemmer av turoperatoren.
 * 
 * @see Turoperator::getAntallTuropplegg()
*/
void Turoperator::skrivEn() const {
    cout << "Gateadresse: " << gateadresse << endl;
    cout << "Postnummer og sted: " << postnr_sted << endl;
    cout << "Kontaktpersonens navn: " << kontaktperson_navn << endl;
    cout << "Mailadresse: " << mailadresse << endl;
    cout << "Webside: " << webside << endl;
    cout << "Telefonnummer: " << telefonnummer << endl;
    cout << "Antall turopplegg laget hittil: " << getAntallTuropplegg() << endl;
}