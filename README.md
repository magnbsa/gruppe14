# Gruppe14

PROG1003
Oblig 3
Vår 2024

Gruppemedlemmer: Magnus Sandvik, Simon Schultz, Folke Jernbert. 

FOR Å KJØRE:
1. Gå til riktig mappe. 
2. Kompileres med: g++ -std=c++11 *.cpp -o output
2b. For å inkludere feilmeldinger: g++ -std=c++11 *.cpp -o output>../forste_test_output.txt 2>&1
3. Kjøres med: ./output

(For at fil skal outputtes må programmet kjøres slutt med 'Q' fra hovedmenyen.)