/**
 * @file    by.cpp
*/

#include <iostream>
#include <string>
#include <algorithm>
#include "by.h"
#include "byer.h"
#include "globalefunksjoner.h"
#include "attraksjon.h"
#include "kirke.h"
#include "severdighet.h"
#include "LesData3.h"
#include "attraksjon.h"
#include "museumsgalleri.h"
extern Byer gByerBase;
using namespace std;

/**
 * Default constructor
 */
By::By() : land(""), attraksjoner{} {}

/**
 * Destructor
*/
By::~By() {
    for (auto &attraksjon : attraksjoner) {
        delete attraksjon;
    }
    attraksjoner.clear();
}

/**
 * Funksjon som legger til attraksjon til bymedlemmets attraksjonsliste.
 * 
 * @param   attraksjon  -   peker til attraksjon som legges til i
 *                          attraksjoner-listen i byens privatmedlem. 
*/
void By::leggTilAttraksjon(Attraksjon* attraksjon) {
    attraksjoner.push_back(attraksjon);
}

/**
* Skriver ut data om alle byene i ByerBase. Alt skrives ut p� en linje.
*
* @see attraksjon.cpp
*/
void By::skrivDataAlle() const{
     Attraksjon* attraksjon;
     cout << "(" << land << ")";
     cout << "\tAntall attraksjoner: " << attraksjoner.size();
}

/**
 * Brukes i Byer En som skriver ut all data om en by.
 *
 * @see attraksjon.cpp for skrivData
*/
void By::skrivData() const {
    Attraksjon* attraksjon;
    cout << "(" << land << ")";
    cout << "\n\nATTRAKSJONER I DENNE BYEN\n\n";
    if (!attraksjoner.empty()) {
        for_each(attraksjoner.begin(),attraksjoner.end(),
                 [](Attraksjon* attraksjon){

                if(dynamic_cast<Museumsgalleri*>(attraksjon)!= nullptr){
                  cout << "Museum/Galleri\n";
                }else if(dynamic_cast<Kirke*>(attraksjon)!= nullptr){
                  cout << "Kirke\n";
                }else if(dynamic_cast<Severdighet*>(attraksjon)!= nullptr){
                  cout << "Severdighet\n";
                }

                attraksjon->skrivData();
                cout << "\n";
                 }
);}

}

/**
 * Funksjon som spør bruker om attraksjon og fjerner denne. 
*/
void By::fjernOnsketAttraksjon() {
    int indeks = 1;
    for (auto& attraksjon : attraksjoner) {
        cout << indeks << ". " << attraksjon->hentID() << endl;
        indeks++;
    }

    int nr = lesInt("Hvilken attraksjon vil du slette? Oppgi nr.: ", 1, attraksjoner.size());

    auto it = attraksjoner.begin();
    advance(it, nr-1);
    delete *it;
    attraksjoner.erase(it);

}

/**
 * Funksjon som henter det private medlemmet land til byobjektet.
 *
 * @return  Landet til byen.
*/string By::hentLand() const {
    return land;
}

/**
 * Funksjon som henter det private medlemmet attraksjoner til byobjektet.
 *
 * @return  Liste av attraksjoner knyttet til byen.
*/
list <Attraksjon*> By::hentAttraksjoner() const {  // Til bruk i turopplegg.cpp
    return attraksjoner;
}