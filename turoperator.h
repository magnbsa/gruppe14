#ifndef TUROPERATOR_H
#define TUROPERATOR_H

#include <string>
#include "turopplegg.h"

class Turoperator {
    private:
        int antall_opplegg; // Denne er for antall opplegg registrert. 
        std::string gateadresse;
        std::string kontaktperson_navn;
        std::string mailadresse;
        int oppleggID;  // Denne er for å telle opplegg skrevet til fil.
        std::string postnr_sted;
        int telefonnummer;
        std::string webside;
        std::vector<Turopplegg*> opplegg;

    public:
        Turoperator();
        Turoperator(const std::string& gateadresse,
                    const std::string& postnr_sted,
                    const std::string& kontaktperson_navn,
                    const std::string& mailadresse,
                    const std::string& webside,
                    int telefonnummer,
                    std::vector<Turopplegg*> opplegg,
                    int oppleggID,
                    int antall_opplegg);
        ~Turoperator();
        int getAntallTuropplegg() const;
        std::string getGateadresse() const;
        std::string getKontaktperson_navn() const;
        std::string getMailadresse() const;
        std::string getPostnr_sted() const;
        int getTelefonnummer() const;
        std::string getWebside() const;
        int hentOppleggID() const;
        void endre();
        void inkrementerOppleggID();
        void leggTilOpplegg(const std::string& turoperator_navn, int oppleggID);
        void skrivEn() const;
};

#endif