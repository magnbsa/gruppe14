#ifndef ATTRAKSJON_H
#define ATTRAKSJON_H


#include <string>

/**
* Attraksjon med ID, beskrivelse, adresse og webside.
*/


class Attraksjon {
    protected:
        std::string ID; // Dette er et unikt navn, i. e. "Eiffeltårnet".
        std::string beskrivelse;
        std::string adresse;
        std::string webside;
    public:
        Attraksjon(std::string ID);
        Attraksjon(std::string ID, std::string beskrivelse, std::string adresse, std::string webside);
        virtual         ~Attraksjon();
        std::string     hentAdresse() const;
        std::string     hentBeskrivelse() const;
        std::string     hentID() const;
        std::string     hentWebside() const;
        virtual void    lesData();
        virtual void    skrivData() const;
};
#endif // ATTRAKSJON_H
