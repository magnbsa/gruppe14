/**
 *   Hovedprogrammet for OOP-prosjektet V24 med turoperat�rer, byer,
 *   attraksjoner i byene og N dagers ulike reiseopplegg for turoperat�rene.
 *
 *   Denne inneholder også definisjon av de globale variablene.
 *
 *   Når globale objekter trengs å bli brukt i andre filer,
 *   refereres de til vha. extern... gjelder gOperatorBase og gByerBase
 *
 *   @file     MAINV24.CPP
 *   @author   Frode Haug, NTNU
 */


#include <iostream>
#include <map>
#include <vector>
#include <string>
#include "turoperatorer.h"
#include "byer.h"
#include "turopplegg.h"
#include "globalefunksjoner.h"
#include "LesData3.h"
using namespace std;

Turoperatorer gOperatorBase; ///<Globalt container-objekt med ALLE operatørerene
Byer gByerBase;              ///<Globalt container-objekt med ALLE byene.

//  Kode som legges innledningsvis i de .cpp-filene som trenger � bruke
//  en eller begge det to globale objektene definert ovenfor:
//         extern Turoperatorer gOperatorBase;
//         extern Byer gByerBase;

/**
 *  Hovedprogram.
*/
int main()  {
    char valg;

    gOperatorBase.lesFraFil();
    gByerBase.lesFraFil();

    skrivMeny();

    while (valg != 'Q') {
    valg = lesChar("Kommando");

    switch(valg) {
       case 'T': handlingT();       break;
       case 'B': handlingB();       break;
       case 'A': handlingA();       break;
       default: skrivMeny();        break;

}
        skrivMeny();
}


    gOperatorBase.skrivTilFil();
    gByerBase.skrivTilFil();

    cout << "\n\n";
    return 0;
}
