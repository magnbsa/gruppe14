#include <iostream>
#include <string>
#include "museumsgalleri.h"
#include "globalefunksjoner.h"
using namespace std;

/**
* Default constructor.
*
* Constructor som initialiserer et museumsgalleri med en spesifikk ID(navn).
*
* @param ID
*/
Museumsgalleri::Museumsgalleri(string ID) : Attraksjon (ID){
     hoydepunkt = "";
}


/**
* Destructor.
*
* Frigjør ressurser tildelt museumsgalleri.
*/
Museumsgalleri::~Museumsgalleri(){}


/**
* Denne funksjonen ber brukeren først om lese inn data fra Attraksjon-klassen.
* Deretter må brukeren lese inn et høydepunkt fra museumsgalleriet.
*
* @see lesTekst() i globalefunksjoner.cpp
* @see Attraksjon::lesData()
*/
void Museumsgalleri::lesData(){
    Attraksjon::lesData();

    hoydepunkt=lesTekst("Skriv inn høydepunkt(er):");
}


/**
* Denne funksjonen skriver først ut all data fra valgt attraksjon, deretter
* blir det skrevet ut et høydepunkt fra denne attraksjonen.
*
* @see Attraksjon::skrivData()
*/
void Museumsgalleri::skrivData() const{
    Attraksjon::skrivData();

    cout << "Høydepunkt:" << hoydepunkt << "\n";
}
