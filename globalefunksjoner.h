#ifndef GLOBALEFUNKSJONER_H
#define GLOBALEFUNKSJONER_H


/**
 * En fil med alle globale funksjonsheadinger.
 *
 * @file    globalefunksjoner.H
 * @authors Magnus B. Sandvik, Simon S. Aasheim, Folke Jernbert.
*/
#include <string>
#include <iostream>

void skrivMeny();
void skrivMenyTur();
void skrivMenyBy();
void skrivMenyAtt();
std::string lesTekst(std::string tekst);
void handlingA();
void handlingB();
void handlingT();
/* static void inkrementerOppleggID();
static int hentOppleggID(); */
std::string stringToUpper(std::string tekst);
std::string toLower(const std::string& str);



#endif // GLOBALEFUNKSJONER_H
