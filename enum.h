/**
 * En fil med alle enum'er.
 *
 * @file    enum.H
 * @authors Magnus B. Sandvik, Simon S. Aasheim, Folke Jernbert.
*/
#ifndef ENUM_H
#define ENUM_H


enum KirkeType {Katedral, eKirke, Kapell, Ukjent};

#endif // ENUM_H
