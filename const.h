/**
 * En fil med alle const'er.
 *
 * @file    const.H
 * @authors Magnus B. Sandvik, Simon S. Aasheim, Folke Jernbert.
*/

#ifndef CONST_H
#define CONST_H



const int MAXBYGGEAR = 2023;
const int MAXKIRKEPLASS = 300;
const int MAXDAGER = 4;
const int MINBYGGEAR = 0;
const int MINKAPASITET = 50;
const int MAXKAPASITET = 2000;

// Til Turoperator
const int MINTLF = 10000000; // tillater ikke telefonnr som starter på 0
const int MAXTLF = 99999999;

// Til Turopplegg
const int OPPLEGG_MINDAGER = 1;
const int OPPLEGG_MAXDAGER = 30;


#endif // CONST_H
