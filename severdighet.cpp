/**
 * 8. Severdighet (subklasse av Attraksjon):
• string: spesielt å se/gjøre/oppleve ved/på dette stedet
*/
#include <iostream>
#include <string>
#include "severdighet.h"
#include "globalefunksjoner.h"
using namespace std;


/**
* Default constructor.
*
* Constructor som initialiserer en severdighet med en spesifikk ID(navn).
*
* @param ID
*/
Severdighet::Severdighet(string ID) : Attraksjon (ID) {
    spesiell_opplevelse="";
}


/**
* Destructor.
*
* Frigjør ressurser tildelt severdighet.
*/
Severdighet::~Severdighet(){}


/**
* Denne funksjonen ber brukeren først om lese inn data fra Attraksjon-klassen.
* Deretter må brukeren lese inn en spesiell opplevelse fra severdigheten.
*
* @see lesTekst() i globalefunksjoner.cpp
* @see Attraksjon::lesData()
*/
void Severdighet::lesData(){
    Attraksjon::lesData();

    spesiell_opplevelse =lesTekst("Skriv inn en spesiell opplevelse:");
}


/**
* Denne funksjonen skriver først ut all data fra valgt attraksjon, deretter
* blir det skrevet ut en spesiell opplevelse fra denne attraksjonen.
*
* @see Attraksjon::skrivData()
*/
void Severdighet::skrivData() const{
    Attraksjon::skrivData();

    cout << "Spesiell opplevelse:" << spesiell_opplevelse << "\n";
}
