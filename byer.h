#ifndef BYER_H
#define BYER_H

#include <map>
#include <string>
#include "by.h"


class Byer {
    private:
        std::map <std::string, By*> byer;
    public:
        Byer();
        ~Byer();
        std::map<std::string, By*> hentByer() const;
        std::string hentNavn(By* ptr) const;
        void nyBy();
        void leggTilBy(std::string bynavn, By* by);
        void lesFraFil();
        void skrivTilFil();
        void fjernBy();
        void fjernAttraksjon();
        void skrivEn() const;
        void skrivAlle() const;
        void nyAttraksjon();
        By* finnBy(const std::string& navn) const;
};
#endif // BYER_H
