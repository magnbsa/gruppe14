#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include "turoperatorer.h"
#include "turoperator.h"
#include "LesData3.h"
#include "globalefunksjoner.h"
using namespace std;
extern Turoperatorer gOperatorBase;

/**
 * Default constructor.
*/
Turoperatorer::Turoperatorer() {}

/**
 * Destructor.
*/
Turoperatorer::~Turoperatorer() {
    for (auto& entry : operatorer) {
        delete entry.second;
    }
    operatorer.clear();
}

/**
 * Funksjon som tar en brukerinputtet turoperatornavn som parameter og sjekker
 * om den entydig, case-insensitivt bestemmer en turoperatør i mappen. Hvis så, 
 * returnerer peker til turoperatoren. Hvis ikke, returnerer nullpeker.
 * 
 * @param   navn            -   streng med brukerinput av turoperatornavn.
 * @return  Turoperator*    -   peker til Turoperatorobjekt.
 * @see toLower() i globalefunksjoner.cpp
*/
Turoperator* Turoperatorer::finnTuroperator(const string& navn) const {
    Turoperator* funn = nullptr;
    int matchTeller = 0;    // Teller antall match.

    string inputLower = toLower(navn);

    for (const auto& pair : operatorer) {
        if (pair.first.find(inputLower) == 0) { // fant match.
            funn = pair.second;
            ++matchTeller;
            if (matchTeller > 1)
                break;
        }
    }
    if (matchTeller > 1)
        funn = nullptr;     // Invaliderer siden ikke unik.
    return funn;
}

/**
 * Funksjon som tar en turoperatør-peker som parameter og returnerer
 * navnet på den turoperatøren fra turoperatorer-mappen. 
 * 
 * @param   ptr   -   Turoperator-peker.
 * @return  navn på turoperatør. 
*/
string Turoperatorer::hentNavn(Turoperator* ptr) {
    string operatornavn;
    for (const auto& pair : operatorer) {
        if (pair.second == ptr) {
            operatornavn = pair.first;
            break;
        }
    }
    return operatornavn;
}

/**
 * Funksjon som returnerer mappen (private medlemmet) i
 * Turoperatorer-klassen.
 *
 * @return  Map inneholdende navn og korresponderende turoperatorpeker.
*/
map<string, Turoperator*> Turoperatorer::hentTuroperatorer() {
    return operatorer;
}

/**
 * Funksjon som finner ønsket turoperator og kaller endre-funksjon på den.
 *
 * @see lesTekst(...) i globalefunksjoner.cpp
 * @see Turoperatorer::finnTuroperator(...)
 * @see Turoperator::endre()
*/
void Turoperatorer::endreTuroperator() {
    string navn = lesTekst("Skriv inn navnet på turoperatøren du ønsker å endre: ");

    Turoperator* turoperator = finnTuroperator(navn);
    if (turoperator != nullptr) {
        turoperator->endre();
    } else {
        cout << "Fant ingen turoperatør med navn '" << navn << "'.\n";
    }
}

/**
 * Funksjon som finner ønsket turoperatør og sletter den fra turoperatorer-mappen.
 *
 * @see lesTekst(...) in globalefunksjoner.cpp
 * @see Turoperatorer::finnTuroperator(navn)
 * @see Turoperatorer::hentNavn(...) - Funksjon som tar inn turoperatorpeker
 *      og returnerer strengen den mappes til i turoperatorer.
 * @see stringToUpper(...) i globalefunksjoner.cpp
*/
void Turoperatorer::fjernTuroperator() {
    string navn = lesTekst("Skriv inn navnet på turoperatøren du ønsker å endre: ");

    Turoperator* turoperator = finnTuroperator(navn);
    string turoperatorNavn = hentNavn(turoperator);
    if (turoperator != nullptr) {
        char bekreft;
        cout << "Er du sikker på at du vil fjerne turoperatøren '" << stringToUpper(turoperatorNavn) << "'? (J/N)";
        bekreft = lesChar("");
        if (bekreft == 'J') {
            operatorer.erase(turoperatorNavn);
            // I oppgavebeskrivelsen som ekstraoppgave å fjerne også turoperatøren fra filene, antageligvis både fra turoperatører.dta og alle turoppleggsfiler som denne turoperatør er assosiert med. Dette er ikke implementert, men skulle i så fall hvert her.
            cout << "Turoperatøren '" << stringToUpper(turoperatorNavn) << "' er fjernet.\n";
        } else {
            cout << "Fjerning avbrutt.\n";
        }
    } else {
        cout << "Fant ingen turoperatør med navn '" << stringToUpper(turoperatorNavn) << "'.\n";
    }
}

/**
 *  Funksjon som finner turoperatør å lage opplegg for og kaller
 *  dens leggTilOpplegg-metode
 *
 *  @see lesTekst(...) in globalefunksjoner.cpp
 *  @see Turoperatorer::finnTuroperator(...)
 *  @see Turoperator::hentOppleggID()
 *  @see Turoperator::leggTilOpplegg(...)
*/
void Turoperatorer::lagOpplegg() {
    string navn = lesTekst("Hvilken turoperatør vil du lage opplegg for?: ");
    Turoperator* turoperator = finnTuroperator(navn);
    navn = hentNavn(turoperator);   // Henter fullstendig navn.
    if (turoperator == nullptr)
        cout << "Fant ingen turoperatør med det navnet!\n";
    else {
        int oppleggID = turoperator->hentOppleggID();
        turoperator->leggTilOpplegg(navn, oppleggID);
    }
}

/**
 * Denne leser inn all informasjon UNNTATT antall turopplegg, som burde lese fra et annet filsystem knyttet til turopplegg.
 * 
 * @see Turoperatorer::Turoperatorer()
*/
void Turoperatorer::lesFraFil() {
    ifstream innfil("turoperatorer.dta");
    if (innfil) {
        int antall_operatorer;
        string linje;

        string navn, gateadresse, postnr_sted, kontaktperson_navn, mailadresse, webside;
        int telefonnummer, oppleggID, antall_opplegg;

        innfil >> antall_operatorer;
        getline(innfil, linje);    // Leser resten av linja ut.

        for (int i = 0; i < antall_operatorer; i++) {
            getline(innfil, navn);
            innfil >> antall_opplegg;   getline(innfil, linje);
            getline(innfil, gateadresse);
            getline(innfil, postnr_sted);
            getline(innfil, kontaktperson_navn);
            getline(innfil, mailadresse);
            getline(innfil, webside);
            innfil >> telefonnummer;    innfil.ignore();
            innfil >> oppleggID;    // Dette er antall opplegg skrevet til fil.
            innfil.ignore();  // Forbereder til neste blokk.

            Turoperator* nyOperator = new Turoperator(gateadresse, postnr_sted, kontaktperson_navn, mailadresse, webside, telefonnummer, {}, oppleggID, antall_opplegg);
            gOperatorBase.operatorer[toLower(navn)] = nyOperator;
        }
        innfil.close();
    } else {
        cerr << "Kunne ikke åpne filen for lesing.\n";
    }
}

/**
 * Funksjon som lager ny turoperatør, sjekker om navnet eksisterer og fyller inn
 * alle private medlemmer. Turoperatør-objektet blir så lagt til i 
 * turoperatorer-mappen.
 * 
 * @see Turoperatorer::finnBy(...)
 * @see Turoperator::Turoperator(...)
 * @see stringToUpper(...) i globalefunksjoner.cpp
*/
void Turoperatorer::nyTuroperator() {
    string navn, gateadresse, postnr_sted, kontaktperson_navn, mailadresse, webside;
    int telefonnummer, oppleggID, antall_turopplegg;

    cout << "Skriv inn navnet på den nye turoperatøren: ";
    getline(cin, navn);

    // Sjekker om turoperatøren allerede eksisterer
    Turoperator* turoperator = finnTuroperator(navn);
    if (turoperator == nullptr) {
        // Samler resten av detaljene
        cout << "Skriv inn gateadressen: ";
        getline(cin, gateadresse);

        cout << "Skriv inn postnummer og sted: ";
        getline(cin, postnr_sted);

        cout << "Skriv inn navnet på kontaktpersonen: ";
        getline(cin, kontaktperson_navn);

        cout << "Skriv inn e-postadressen: ";
        getline(cin, mailadresse);

        cout << "Skriv inn nettsiden: ";
        getline(cin, webside);

        telefonnummer = lesInt("Skriv inn tlf (5-8 siffer): ", 10000, 99999999);

        // Oppretter et nytt Turoperator-objekt og legger det til i mappen
        Turoperator* nyOperator = new Turoperator(gateadresse, postnr_sted, kontaktperson_navn, mailadresse, webside, telefonnummer, {}, 1, 0);
        operatorer[toLower(navn)] = nyOperator;

        cout << "Turoperatøren '" << stringToUpper(navn) << "' er vellykket lagt til.\n";
    } else {
        cout << "En turoperatør med dette navnet eksisterer allerede.\n";
    }
}

/**
 * Funksjon som skriver ut navn, antall turopplegg og webside til alle turoperatorer i turoperatormappen. 
 * 
 * @see stringToUpper i globalefunksjoner.cpp
 * @see Turoperator::getAntallTuropplegg()
 * @see Turoperator::getWebside()
*/
void Turoperatorer::skrivAlle() const {
    for (const auto& entry : operatorer) {
        cout << "Navn: " << stringToUpper(entry.first)
                  << ", Antall turopplegg: " << entry.second->getAntallTuropplegg()
                  << ", Webside: " << entry.second->getWebside()
                  << endl;
    }
}

/**
 * Funksjon som ber bruker om navn på turoperatør, finner turoperatøren i mappen
 * og så skriver ut informasjon ved hjelp av Turoperator::skrivEn()
 * 
 * @see lesTekst(...) i globalefunksjoner.cpp
 * @see Turoperatorer::finnTuroperator(...)
 * @see Turoperatorer::hentNavn(...)    -   Henter navn til turoperator.
 * @see stringToUpper(...) i globalefunksjoner.cpp
 * @see Turoperator::skrivEn()
*/
void Turoperatorer::skrivEnTuroperator() {
    string navn = lesTekst("\nHvilken turoperatør vil du skrive ut: ");
    Turoperator* turoperator = finnTuroperator(navn);
    if (turoperator != nullptr) {
        navn = hentNavn(turoperator);
        cout << "\nNavn: " << stringToUpper(navn) << endl;
        turoperator->skrivEn();
    } else {
        cout << "\nFant ingen turoperatør med navnet '" << navn << "'." << endl;
    }
}

/**
 * Dette opplegget skriver ut all informasjon UNNTATT den knyttet til turoperatørens turopplegg. Det finnes et eget filoppsett for turoperatørers turopplegg. Den tar bare med antall turopplegg som vist testdataeksempelet i oppgavebeskrivelsen.
 *
 * @see Turoperator::getAntallTuropplegg() 
 * @see Turoperator::getGateadresse() 
 * @see Turoperator::getPostnr_sted() 
 * @see Turoperator::getKontaktperson_navn() 
 * @see Turoperator::getMailadresse() 
 * @see Turoperator::getWebside() 
 * @see Turoperator::getTelefonnummer() 
 * @see Turoperator::hentOppleggID() 
 *
*/
void Turoperatorer::skrivTilFil() {
    ofstream utfil ("turoperatorer.dta");
    if (utfil) {
        utfil << operatorer.size() << " stk. turoperatører:\n";
        for (const auto& entry : operatorer) {
            utfil << stringToUpper(entry.first) << "\n" // upper-caser navnestringen.
                << entry.second->getAntallTuropplegg() << " opplegg\n"
                << entry.second->getGateadresse() << "\n"
                << entry.second->getPostnr_sted()<< "\n"
                << entry.second->getKontaktperson_navn() << "\n"
                << entry.second->getMailadresse() << "\n"
                << entry.second->getWebside() << "\n"
                << entry.second->getTelefonnummer() << "\n"
                << entry.second->hentOppleggID() << endl;
        }
        utfil.close();
    } else {
        cerr << "Kunne ikke åpne filen for skriving.\n";
    }
}