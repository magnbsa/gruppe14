#ifndef SEVERDIGHET_H
#define SEVERDIGHET_H

#include <string>
#include "attraksjon.h"

class Severdighet : public Attraksjon {
    private:
        std::string spesiell_opplevelse;

    public:
        Severdighet(std::string ID);
        ~Severdighet();

        virtual void lesData();
        virtual void skrivData() const;

};

#endif
