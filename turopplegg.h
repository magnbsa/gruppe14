/**
 * Vi lager to structer, en dagsplan og en turplan. Dagsplanen holder en by-string og en liste av attraksjoner som skal besøkes i løpet av dagen. Det er ikke implementert at man kan besøke flere byer i løpet av en dag. Turplanen inneholder så en turbeskrivelse-streng og en vector av attraksjoner. Tanken er at turplan-structen skal inneholde all informasjon knyttet til turopplegg, som så kan legges inn i oppleggsvectorer i turoperator-klassen.
*/

#ifndef TUROPPLEGG_H
#define TUROPPLEGG_H

#include <iostream>
#include <string>
#include <vector>
#include <list>

class By;   // SJEKK: om du trenger denne.
class Byer; // For finnBy()
class Attraksjon;   // For hentAttraksjoner()

struct Dagsplan {
    std::string by;
    std::list<Attraksjon*> attraksjoner;
};

struct Turplan {
    std::string beskrivelse;
    std::vector<Dagsplan*> dager;    // Kunne vurdert å gjøre denne til liste.
};

class Turopplegg {
private:
    struct Turplan turplan;

public:
    Turopplegg();
    std::string lagFilnavn(const std::string& turoperatorNavn, int antallDager, int oppleggID);
    bool planOpplegg(Turopplegg* nyTuropplegg, const std::string& operatornavn, int oppleggID);
    std::list<Attraksjon*> velgAttraksjoner(By* by);
    void skriv(const Turplan& plan, const std::string& filnavn, const std::string& operatornavn) const;
};

#endif
