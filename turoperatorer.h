#ifndef TUROPERATORER_H
#define TUROPERATORER_H

#include <map>
#include <string>
#include "turoperator.h"

class Turoperatorer {
private:
    std::map<std::string, Turoperator*> operatorer;

public:
    Turoperatorer();
    ~Turoperatorer();
    Turoperator* finnTuroperator(const std::string& navn) const;
    std::string hentNavn(Turoperator* ptr);
    std::map<std::string, Turoperator*> hentTuroperatorer();
    void endreTuroperator(); 
    void fjernTuroperator();
    void lagOpplegg();
    void lesFraFil();
    void nyTuroperator(); 
    void skrivAlle() const;
    void skrivEnTuroperator(); 
    void skrivTilFil();
};

#endif
