#ifndef MUSEUMSGALLERI_H
#define MUSEUMSGALLERI_H

#include <string>
#include "attraksjon.h"

class Museumsgalleri : public Attraksjon {
    private:
        std::string hoydepunkt;

    public:
        Museumsgalleri(std::string ID);
        ~Museumsgalleri();

        virtual void lesData();
        virtual void skrivData() const;

};

#endif
