/**
 * En fil med definisjoner (innmaten) av alle de globale funksjonene.
 *
 * @file    globalefunksjoner.cpp
 * @authors Magnus B. Sandvik, Simon S. Aasheim, Folke Jernbert.
*/
#include <iostream>
#include <algorithm>        // for transform-metoden
#include "globalefunksjoner.h"
#include "LesData3.h"
#include "byer.h"
#include "turoperatorer.h"
#include <algorithm>
using namespace std;
extern Turoperatorer gOperatorBase;
extern Byer gByerBase;

// static int oppleggID = 0;   // Teller antall turopplegg skrevet til fil.
// void inkrementerOppleggID() { ++oppleggID; }
// int hentOppleggID() { return oppleggID; }

void handlingT() {
        char valg;
        skrivMenyTur();
        valg = lesChar("Kommando");
         switch(valg){
            case 'A':   gOperatorBase.skrivAlle();               break;
            case '1':   gOperatorBase.skrivEnTuroperator();      break;
            case 'N':   gOperatorBase.nyTuroperator();           break;
            case 'E':   gOperatorBase.endreTuroperator();        break;
            case 'F':   gOperatorBase.fjernTuroperator();        break;
            case 'O':   gOperatorBase.lagOpplegg();              break;
            default:    skrivMenyTur();                          break;
}

}

void handlingB(){
        char valg;
        skrivMenyBy();
        valg = lesChar("Kommando");
          switch(valg){
            case 'A':   gByerBase.skrivAlle();                   break;
            case '1':   gByerBase.skrivEn();                     break;
            case 'N':   gByerBase.nyBy();                        break;
            case 'F':   gByerBase.fjernBy();                     break;
            default:    skrivMenyBy();                           break;
}

}


void handlingA(){
        char valg;
        skrivMenyAtt();
        valg = lesChar ("Kommando");
          switch(valg){
            case 'N':   gByerBase.nyAttraksjon();                break;
            case 'F':   gByerBase.fjernAttraksjon();             break;
            default:    skrivMenyAtt();                          break;

}



}




void skrivMeny(){
    cout << "\nVelg mellom Turoperatør, By og Attraksjon: \n"
         << "\tT - Turoperatør\n"
         << "\tB - By\n"
         << "\tA - Attraksjon\n"
         << "\tQ - Avslutte\n";

}


void skrivMenyTur() {
    cout << "\nDine turoperatørvalg: \n"
         << "\tA - Skriv alle turoperatører\n"
         << "\t1 - Skriver en turoperatør\n"
         << "\tN - Leser inn ny turoperatør\n"
         << "\tE - Endrer turoperatør\n"
         << "\tF - Fjerner turoperatør\n"
         << "\tO - Lager ett opplegg\n";
}

void skrivMenyBy(){
    cout << "\nDine byvalg: \n"
         << "\tA - Skriv alle byer\n"
         << "\t1 - Skriver en by\n"
         << "\tN - Leser inn ny by\n"
         << "\tF - Fjerner by\n";
}

void skrivMenyAtt(){
    cout << "\nDine attraksjonsvalg: \n"
         << "\tN - Leser inn ny attraksjon\n"
         << "\tF - Fjerner attraksjon\n";



}


/**
* Brukes til � lese inn tekst til lesData funksjoner
*
* @param tekst
* @return innlest tekst fra bruker.
*/

string lesTekst(string tekst){
       cout << tekst ;
       string verdi;
       getline(cin,verdi);
       return verdi;
}

/**
 * Denne endrer på selve originalen tror jeg. Lager ikke en kopi. Noe å vurdere.
 *
 * @param     tekst  -      det som skal caps lockes.
 * @return    Uppercaset streng.
*/
string stringToUpper(string tekst) {
       transform(tekst.begin(), tekst.end(), tekst.begin(), ::toupper);
       return tekst;
}

/**
 * Funksjon som returnerer lowercaset streng
*/
string toLower(const string& str) {
    string lowerStr = str;
	transform(lowerStr.begin(), lowerStr.end(), lowerStr.begin(),
              [](unsigned char c){ return tolower(c);});
	return lowerStr;
}