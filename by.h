#ifndef by_h
#define by_h


#include <list>
#include <string>
#include "attraksjon.h"

class By {
    private:
        std::string land;
        std::list<Attraksjon*> attraksjoner;
    public:
        By();
        By(std::string land, std::list<Attraksjon*> attraksjoner) : land(land), attraksjoner(attraksjoner) {}
        ~By();
        void leggTilAttraksjon(Attraksjon* attraksjon);
        void skrivData() const;
        void skrivDataAlle() const;
        void fjernOnsketAttraksjon();
        std::string hentLand() const;
        std::list <Attraksjon*> hentAttraksjoner() const;   // Til bruk i turopplegg.cpp

};

#endif

