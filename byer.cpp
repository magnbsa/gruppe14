#include <iostream>
#include <string>
#include <algorithm>    // remove
#include <fstream>  // lesFraFil(), skrivTilFil()
#include "globalefunksjoner.h"  // lesTekst(...), toLower()
#include "byer.h"   // finnBy(...)
#include "by.h"     // hentAttraksjoner()
#include "LesData3.h"   // lesChar(...)
#include "attraksjon.h"     // hentID()
#include "museumsgalleri.h"
#include "kirke.h"
#include "severdighet.h"
using namespace std;

/**
 * Default constructor.
*/
Byer::Byer() {}

/**
 * Destructor.
*/
Byer::~Byer() {
    for (auto& entry : byer) {
        delete entry.second;
    }
    byer.clear();
}

/**
 * Funksjon som returnerer mappen (private medlemmet) i
 * Turoperatorer-klassen.
 *
 * @return  Map inneholdende navn og korresponderende turoperatorpeker.
*/
map<string, By*> Byer::hentByer() const {
    return byer;
}

/**
 * Funksjon som henter keyen i det private medlemmet byer-mappen i Byer-klassen.
 *
 * @param   -   Peker til Byobjekt.
 * @return  -   Korresponderende navnestreng i Byer-mappen.
*/
string Byer::hentNavn(By* ptr) const {
    string bynavn;
    for (const auto& pair : byer) {
        if (pair.second == ptr) {
            bynavn = pair.first;
            break;
        }
    }
    return bynavn;
}

/**
 * Funksjon som legger til et par i byer-mappen.
 * Bynavn lagres lowercaset.
 *
 * @param   -   Bynavn streng.
 * @param   -   Peker til byobjekt.
 * @see toLower() i globalefunksjoner.cpp
*/
void Byer::leggTilBy(std::string bynavn, By* by) {
    byer[toLower(bynavn)] = by;
}

/**
 * Leser byobjekt fra "byer.dta"-fil og legger til i byer-mappen.
 *
 * Formatet på filen er:
 *  2 stk. byer:
 *  MILANO, Italia
 *  Attraksjon1
 *  Attraksjon2
 *  Attraksjon3\n
 *  PARIS, Frankrike
 *      også videre...
 *
 * @see Byer::leggTilBy(...)
 * @see toLower(...) in globalefunksjoner.cpp
*/
void Byer::lesFraFil() {
    ifstream fil("byer.dta");
    if (fil.is_open()) {
        // Første linje i fil
        string linje;
        char antall_byer_char;
        fil >> antall_byer_char;
        int antall_byer = antall_byer_char - '0';   // Konverterer char til int.
        getline(fil, linje);	// Leser ut linjen inkludert \n.
    // Loop gjennom byobjektene
    string bynavn, land;
    list<Attraksjon*> attraksjoner;
    for (int i = 0; i < antall_byer; i++) {
        // Første linje med bynavn, land
        fil >> bynavn;
        // Bynavn inneholder nå et komma på slutten som vi fjerner.
        bynavn.erase(remove(bynavn.begin(), bynavn.end(), ','), bynavn.end());
        fil.ignore();	// Ignorer mellomrom.
        fil >> land;    fil.ignore();   // Ignorerer newline.
        attraksjoner.clear();

        string beskrivelse, adresse, webside;
        // Linjene med attraksjoner
        while (getline(fil, linje)) {	// Byer separert med \n.
            if (linje.empty())
                break;
            getline(fil, beskrivelse); 
            getline(fil, adresse);
            getline(fil, webside);
            Attraksjon* attraksjon = new Attraksjon(linje, beskrivelse, adresse, webside);
            attraksjoner.push_back(attraksjon);
        }
        By* by = new By(land, attraksjoner);
        leggTilBy(toLower(bynavn), by);
    }
        fil.close();
    } else
        cout << "Kunne ikke åpne fil!" << endl;
}

/**
* Funksjon som Lager ny by
*
* @see  lesTekst(...) i globalefunksjoner.cpp
* @see  Byer::finnBy(...)
* @see  toLower(...) i globalefunksjoner.cpp
* @see  leggTilBy(...) i byer.cpp
*/
void Byer::nyBy() {
    string bynavn;
    do {
    bynavn = lesTekst("Skriv inn navnet på byen: ");
    } while (finnBy(bynavn) != nullptr);

    string land = lesTekst("Skriv inn navnet på landet: ");
    By* by = new By(land, {});  // Attraksjonslista initialiseres tom.

    bynavn = toLower(bynavn);   // lower-caser før lagring
    leggTilBy(bynavn, by);
}

/**
 * Skriver til fil "byer.dta"
 *
 * Formatet skal være:
 *  2 stk. byer:
 *  MILANO, Italia
 *  Attraksjon1
 *  Attraksjon2
 *  Attraksjon3
 *      også videre...
 *
 * @see global stringToUpper(...)
 * @see By::hentLand(), hentAttraksjoner()
 * @see Attraksjon::hentID()
 * @see Attraksjon::hentAdresse()
 * @see Attraksjon::hentWebside()
 * @see Attraksjon::hentBeskrivelse()
*/
void Byer::skrivTilFil() {
    ofstream fil("byer.dta");
    if (fil.is_open()) {
        fil << byer.size() << " stk. byer:" << endl;
        for (auto& by : byer) {
            fil << stringToUpper(by.first) << ", " << by.second->hentLand() << endl;
            for (auto& attraksjon : by.second->hentAttraksjoner()) {
                fil << attraksjon->hentID() << endl;
                fil << attraksjon->hentBeskrivelse() << endl;
                fil << attraksjon->hentAdresse() << endl;
                fil << attraksjon->hentWebside() << endl;
            }
            fil << endl;
        }
    fil.close();
    } else
        cout << "\nKunne ikke åpne fil!\n";
}

/**
* Fjerner en valgt by fra byer
* @see lesTekst(...) in globalefunksjoner.cpp
* @see Byer::finnBy(...)
* @see Byer::hentNavn(...)
*/
void Byer::fjernBy() {
    string navn = lesTekst("Skriv inn navnet på byen du ønsker å fjerne: ");
    By* by = finnBy(navn);
    navn = hentNavn(by);

    if (by != nullptr) {
        char bekreft;
        cout << "Er du sikker på at du vil fjerne byen '" << navn << "'? (J/N): ";
        do {
            bekreft = lesChar("");
            if ((bekreft != 'J') && (bekreft != 'N'))
                cout << "\n Ugyldig valg. Prøv igjen: ";
        } while ((bekreft != 'J') && (bekreft != 'N'));

        if (bekreft == 'J') {
            byer.erase(navn);
            cout << "Byen '" << navn << "' er fjernet.\n";
            delete by;
        } else {
            cout << "Fjerning avbrutt.\n";
        }
    } else {
        cout << "Fant ingen by med navn '" << navn << "'.\n";
    }
}

/**
*  Lager ny attraksjon i en by.
*
* @see finnBy()
*
*/
void Byer::nyAttraksjon(){
    cout << "\nLovlige valg: \n";
    for (const auto & val : byer){
        cout << val.first << "\n";
    }
    string bynavn = lesTekst("\nSkriv inn byen du vil legge til en attraksjon: ");
    By* by = finnBy(bynavn);

        if (by != nullptr){
            Attraksjon* attraksjon = nullptr;
            char attValg;
            auto attraksjoner = by->hentAttraksjoner();
            string ID = lesTekst("Skriv inn navn på attraksjon: ");
            ID = toLower(ID);
            //Sjekker om attraksjonen finnes fra før
            bool finnesAtt = any_of(attraksjoner.begin(), attraksjoner.end(),
                                    [&ID](Attraksjon* attraksjon){
                                    return attraksjon->hentID() == ID;
            });

            if(!finnesAtt){

            cout << "\nVelg type attraksjon: ";
            attValg=lesChar("\nM(useum/Galleri) K(irke) S(everdighet)");

            switch(attValg){
                case 'M':
                    attraksjon = new Museumsgalleri(ID);
                    break;
                case 'K':
                    attraksjon = new Kirke(ID);
                    break;
                case 'S':
                    attraksjon = new Severdighet(ID);
                    break;
                default:
                    cout << "\nUgyldig valg. Prøv igjen.\n";

            }} else {cout << "Denne attraksjonen finnes fra før!\n";

            }   if(attraksjon){
           attraksjon->lesData();
           by->leggTilAttraksjon(attraksjon);


}}        else{
            cout << "\nFant ingen by med navnet: " << bynavn << "\n";

}
}

/**
 * Fjerner attraksjon fra en by.
 *
 * @see lesTekst(...) in globalefunksjoner.cpp
 * @see By::fjernOnsketAttraksjon(...)
*/
void Byer::fjernAttraksjon() {
    string bynavn = lesTekst("Skriv inn byen du ønsker å slette attraksjon fra:  ");
    By* by = finnBy(bynavn);
    if (by != nullptr) {
        by->fjernOnsketAttraksjon();
    } else
        cout << "Fant ikke by!" << endl;
}

/**
 * Finner ønsket by å skrive ut og kaller skrivData-funksjon fra By-objektet.
 *
 * @see lesTekst(...) i globalefunksjoner.cpp
 * @see Byer::finnBy(...)
 * @see By::skrivData()
*/
void Byer::skrivEn() const {
    string navn = lesTekst("Hvilken by vil du skrive ut? ");
    By* by = finnBy(navn);
        if (by != nullptr){
            cout << "\nBy: " << stringToUpper(navn);
            by->skrivData(); // Bruker by.cpp funksjon til å skrive ut.
        }
        else {
            cout << "\nIngen by funnet med dette navnet\n";
        }
}

/**
 * Funksjon som iterer gjennom Byer-mappen og skriver ut
 * navnestrengen og så kaller By-klassens skrivData for å
 * skrive ut informasjon om den enkelte by.
 *
 * @see By::skrivData()
*/
void Byer::skrivAlle() const {
    for (const auto & by : byer){
        cout << "\n" << stringToUpper(by.first);
        (by.second)->skrivDataAlle();
    } cout << "\n";
}

/**
 * Vi itererer over Byer-mappen og sjekker om brukerinput matcher med starten
 * av keyen. Denne funksjonen forutsetter at byer lagres i lowercase.
 *
 * @param   navn        -   Brukerinputtet bynavn.
 * @return  By*         -   Peker til byobjekt som matcher brukers inputstreng.
 * @see toLower(...) in globalefunksjoner.cpp
*/
By* Byer::finnBy(const string& navn) const {
    By* funn = nullptr;
    int matchTeller = 0;    // Teller antall match.

    string inputLower = toLower(navn);

    for (const auto& pair : byer) {
        if (pair.first.find(inputLower) == 0) { // fant match.
            funn = pair.second;
            ++matchTeller;
            if (matchTeller > 1)
                break;
        }
    }
    if (matchTeller > 1)
        funn = nullptr;     // Invaliderer siden ikke unik.
    return funn;
}
