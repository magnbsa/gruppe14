#include <iostream>
#include <string>
#include "LesData3.h"
#include "kirke.h"
#include "const.h"
#include "globalefunksjoner.h"
using namespace std;


/**
* Default constructor.
*
* Constructor som initialiserer en kirke med en spesifikk ID(navn).
*
* @param ID
*/
Kirke::Kirke(string ID) : Attraksjon (ID) {
    byggear=0;
    kapasitet=0;
    type = Ukjent;
}


/**
* Destructor.
*
* Frigjør ressurser tildelt kirken.
*/
Kirke::~Kirke() {}


/**
* Denne funksjonen ber brukeren først om lese inn data fra Attraksjon-klassen.
* Deretter må brukeren lese inn byggeår og kapasiteten til kirken, samt
* velge mellom tre ulike kirketyper.
*
* @see lesTekst() i globalefunksjoner.cpp
* @see Attraksjon::lesData()
*/
void Kirke::lesData(){
    Attraksjon::lesData();

    byggear=lesInt("Skriv inn byggeår",MINBYGGEAR,MAXBYGGEAR);
    kapasitet=lesInt("Skriv inn kirkens kapasitet",MINKAPASITET,MAXKAPASITET);

    cout << "\n\tVelg mellom kirketypene: katedral, kirke, kapell\n";

    string kirkeValg;
    kirkeValg = lesTekst("Kirketype(små bokstaver):");


    if (kirkeValg == "kirke"){
        type = eKirke;

} else if (kirkeValg =="katedral"){
           type = Katedral;

} else if (kirkeValg =="kapell"){
           type = Kapell;

} else { cout << "Denne kirketypen finnes ikke!\n";


}


}


/**
* Denne funksjonen skriver først ut all data fra valgt attraksjon, deretter
* blir det skrevet ut byggeår, kapasitet og kirketype.
*
* @see Attraksjon::skrivData()
*/
void Kirke::skrivData() const{
    Attraksjon::skrivData();

    cout << "Byggeår:" << byggear << "\n";
    cout << "Kapasitet:" << kapasitet << "\n";

    string typeKirke ="";
    switch (type){
        case Katedral: typeKirke = "Katedral"; break;
        case eKirke:    typeKirke = "Kirke";    break;
        case Kapell:   typeKirke = "Kapell";   break;
        default: typeKirke = "Ukjent";
    }
    cout << "Kirketype: " << typeKirke << "\n";
}
