#include <iostream>
#include <list>
#include <string>
#include <fstream>  // skriv-funksjonen
#include <iomanip>  // for setw og setfill
#include <sstream>  // for konstruksjon av filnavn.
#include "turopplegg.h"
#include "turoperator.h"
#include "byer.h"   // for finnBy(...)
#include "by.h"     // for velgAttraksjoner(...)
#include "LesData3.h"
#include "const.h"
#include "globalefunksjoner.h"
#include "attraksjon.h" // for hentAttraksjoner(...)
using namespace std;

class Attraksjon;

extern Byer gByerBase;

// Default constructor
Turopplegg::Turopplegg() {}

/**
 * @see Turopplegg::planOpplegg()... (??)
 *
 * Funksjon som konstruerer et filnavn for utskriving av turopplegg.
 *
 * @see globalefunksjoner.cpp for hentOppleggID()
*/
string Turopplegg::lagFilnavn(const string& turoperatorNavn, int antallDager, int oppleggID) {
    stringstream ss;
    ss << stringToUpper(turoperatorNavn) << "-NR." << setw(3) << setfill('0') << oppleggID << "-" << antallDager << "dg.dta";
    return ss.str();
}

/**
 * Funksjon som lager et opplegg med turoperator, beskrivelse og et satt antall dager. For hver dag bestemmes en by og en liste attraksjoner. Til slutt skrives opplegget ut på skjermen, og bruker bestemmer om hen også ønsker å skrive opplegget til fil.
 *
 * Har ikke implementert at man kan besøke flere byer på samme dag.
 *
 * @param   nyTuropplegg - Turoppleggpeker fra Turoperator::leggTilOpplegg(...)
 * @param   operatornavn - string med navn på operatør til bruk i skriv(...)
 * @see Byer::finnBy(...)
 * @see Turopplegg::lagFilnavn(...)
 * @see Turopplegg::skriv(...)
*/
bool Turopplegg::planOpplegg(Turopplegg* nyTuropplegg, const string& operatornavn, int oppleggID) {
    bool skrevetUt = false;
    map<string, By*> byer = gByerBase.hentByer();
    if (byer.size() > 0) {
        char kommando;
        string bynavn;
        By* by = nullptr;

        // Henter beskrivelse
        cout << "\nGi en generell beskrivelse av opplegget: ";
        getline(cin, nyTuropplegg->turplan.beskrivelse);

        // Henter antall dager
        int antall_dager = lesInt("Antall dager opplegget varer?: ", OPPLEGG_MINDAGER, OPPLEGG_MAXDAGER);

        // Lager program for hver dag
        for (int i = 0; i < antall_dager; i++) {
                kommando = 'F';    // Initialiserer til 'fortsett'.
                cout << "Dag nr." << i + 1 << endl;
                Dagsplan* nyDagsplan = new Dagsplan();
                do {
                    cout << "Du kan velge mellom følgende byer:\n";
                    // Lambda-funksjon for å skrive ut bynavn.
                    [](const Byer& byer) {
                        for (const auto& pair : byer.hentByer()) {
                            cout << stringToUpper(byer.hentNavn(pair.second)) << endl;
                        }
                    }(gByerBase);
                    bynavn = lesTekst("Skriv inn by: ");
                    by = gByerBase.finnBy(bynavn);
                    if (by == nullptr)
                        cout << "Byen finnes ikke. Prøv igjen.\n";
                    } while (by == nullptr);
                // Henter ekte navn, siden bruker kan skrive utydelig navn.
                bynavn = gByerBase.hentNavn(by);
                nyDagsplan->by = bynavn;

                while (kommando == 'F') {
                    // Bruker velger attraksjoner for dagens by.
                    nyDagsplan->attraksjoner = velgAttraksjoner(by);
                    // Legger til dagsplan til dager-listen.
                    nyTuropplegg->turplan.dager.push_back(nyDagsplan);
                    do {
                        kommando = lesChar("Fortsette på denne dagen (F), Neste dag (N), Avslutte (Q): ");
                    } while (kommando != 'F' && kommando != 'N' && kommando != 'Q');
                }
            if (kommando == 'Q') break;
        }
        cout << "\n";
        skriv(nyTuropplegg->turplan, "", operatornavn); // Sender filnavn tom streng.
        kommando = lesChar("\nØnsker du skrive til fil? (J/N)");
        if (kommando == 'J') {
            skrevetUt = true;
            skriv(nyTuropplegg->turplan, lagFilnavn(operatornavn, antall_dager, oppleggID), operatornavn);
        }
    } else
        cout << "\nIngen byer å lage turopplegg for!";
    return skrevetUt;
}

/**
 * @param   by  -   by med attraksjoner.
 * @return  liste med brukervalgte attraksjoner
 * @see     Attraksjon::hentAttraksjoner()
*/
list<Attraksjon*> Turopplegg::velgAttraksjoner(By* by) {
    list<Attraksjon*> valgteAttraksjoner;

    // henter liste av attraksjoner fra By-klassen.
    auto attraksjoner = by->hentAttraksjoner();

    // Skriver de ut nummerert.
    int indeks = 1;
    cout << "\nDette er attraksjonene du har å velge mellom:\n ";
    for (auto attraksjon : attraksjoner) {
        cout << indeks << ". " << attraksjon->hentID() << "\n";
        ++indeks;
    }

    cout << "\nVennligst skriv inn nummeret på attraksjonene (i rekkefølge!) du ønsker i turopplegget.\nTrykk enter mellom hvert nummer. For å avslutte taster du 0 og enter.\n";

    int valg;
    do {
        valg = lesInt("", 0, attraksjoner.size());
        if (valg > 0) {
        auto it = next(attraksjoner.begin(), valg - 1); // Henter valgte attraksjon
            valgteAttraksjoner.push_back(*it); // Add it to the chosen list
        }
    } while (valg != 0);
    return valgteAttraksjoner;
}

/**
 * Funksjon som enten skriver til skjermen eller til fil avhengig av om den valgfrie andre parameteren blir sendt med. Hviler på en polymorfisme mellom ostream og ofstream og cout. De to sistnevnte er deriverte klasser av ostream.
 *
 * @see lagFilnavn(...)
 * @see globalefunksjoner.cpp
*/
void Turopplegg::skriv(const Turplan& plan, const string& filnavn,
    const string& operatornavn) const {
    ostream* ut = &cout; // Default til cout
    ofstream fil;

    if (!filnavn.empty()) {
        fil.open(filnavn);
        if (fil.is_open()) {
            ut = &fil; // Peker til fil hvis fil åpnet.
        } else {
            cerr << "Kan ikke åpne fil: " << filnavn << endl;
            // Hvis filen ikke åpner, fortsetter vi med cout.
        }
    }

    // Samme utskrift for både skjerm og fil.
    *ut << "Turoperatør: " << stringToUpper(operatornavn) << "\n";
    *ut << "Beskrivelse: " << plan.beskrivelse << "\n";
    for (const auto& dag : plan.dager) {
        *ut << "By: " << stringToUpper(dag->by) << "\n";
        for (const auto& attraksjon : dag->attraksjoner) {
            *ut << "Attraksjon: " << attraksjon->hentID() << "\n";
        }
    }

    if (fil.is_open()) {
        fil.close(); // Lukker bare fil hvis den ble åpnet.
    }
    return;
}