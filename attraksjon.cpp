#include <iostream>
#include <string>
#include "attraksjon.h"
#include "by.h"
#include "globalefunksjoner.h"
using namespace std;


/**
* Default constructor.
*
* Constructor som initialiserer en attraksjon med en spesifikk ID(navn).
*
* @param ID
*/
Attraksjon::Attraksjon(string ID){
    this->ID = ID;
    beskrivelse = "";
    adresse = "";
    webside = "";
}

/**
 * Constructor med parametre. 
*/
Attraksjon::Attraksjon(std::string ID, std::string beskrivelse, std::string adresse, std::string webside)
    : ID(ID), beskrivelse(beskrivelse), adresse(adresse), webside(webside) {

}

/**
* Destructor.
*
* Frigjør ressurser tildelt attraksjonen.
*/
Attraksjon::~Attraksjon(){
}

/**
* Funksjon som returnerer en adresse.
*
* @return adresse
*/
string Attraksjon::hentAdresse() const {
    return adresse;
}

/**
* Funksjon som returnerer en beskrivelse.
*
* @return beskrivelse
*/
string Attraksjon::hentBeskrivelse() const {
    return beskrivelse;
}

/**
* Funksjon som returnerer en string som representerer attraksjonens unike ID.
*
* @return ID
*/
string Attraksjon::hentID() const {
    return ID;
}

/**
* Funksjon som returnerer en webside.
*
* @return webside
*/
string Attraksjon::hentWebside() const {
    return webside;
}

/**
* Funksjon som ber brukeren legge inn data om en Attraksjon
*
* @see lesTekst() i globalefunksjoner.cpp
*/
void Attraksjon::lesData(){
    beskrivelse = lesTekst("Skriv inn en beskrivelse om attraksjonen:");
    adresse = lesTekst ("Skriv inn adressen til attraksjonen:");
    webside = lesTekst ("Skriv inn websiden til attraksjonen:");
}


/**
* Funksjon som skriver ut all data om en attraksjon i en by.
*/
void Attraksjon::skrivData() const{
    cout << "Navn på attraksjon:" << ID << "\n";
    cout << "Beskrivelse om attraksjon:" << beskrivelse << "\n";
    cout << "Adresse til attraksjon:" << adresse << "\n";
    cout << "Webside til attraksjon:" << webside << "\n";
}
