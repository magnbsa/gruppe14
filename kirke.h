#ifndef KIRKE_H
#define KIRKE_H

#include <string>
#include "enum.h"
#include "attraksjon.h"


class Kirke : public Attraksjon {
    private:
        int byggear;
        int kapasitet;
        KirkeType type;

    public:
        Kirke(std::string ID);
        ~Kirke();

        virtual void lesData();
        virtual void skrivData() const;
};

#endif
